<?php

namespace App\Http\Controllers;

use App\Models\Asistentes;
use Illuminate\Http\Request;

class AsistentesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Asistentes::get();
        return response()->json($records);
    }
    public function getAsistente($id)
    {
        $records = Asistentes::whereId($id)->get();
        return response()->json($records);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request['id'];
        if( isset($id) )
        {
            $record = Asistentes::whereId($id)->update($request->all());
            return response()->json(['mensaje' => 'Actualizado correctamente', $record]);
        } else {
            $record = Asistentes::create($request->all());
            return response()->json(['mensaje' => 'Guardado correctamente', $record]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Asistentes  $asistentes
     * @return \Illuminate\Http\Response
     */
    public function show(Asistentes $request)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Asistentes  $asistentes
     * @return \Illuminate\Http\Response
     */
    public function edit(Asistentes $asistentes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Asistentes  $asistentes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Asistentes $asistentes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Asistentes  $asistentes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asistentes $asistentes)
    {
        //
    }
}
