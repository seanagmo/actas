<?php

namespace App\Http\Controllers;

use App\Models\Compromiso;
use Illuminate\Http\Request;

class CompromisoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Compromiso::with(['responsableA'])->get();
        return response()->json($records);
    }
    public function getCompromiso($id)
    {
        $records = Compromiso::whereId($id)->with(['responsableA'])->get();
        return response()->json($records);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request['id'];
        if( isset($id) )
        {
            $record = Compromiso::whereId($id)->update($request->all());
            return response()->json(['mensaje' => 'Actualizado correctamente', $record]);
        } else {
            $record = Compromiso::create($request->all());
            return response()->json(['mensaje' => 'Guardado correctamente', $record]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Compromiso  $compromiso
     * @return \Illuminate\Http\Response
     */
    public function show(Compromiso $compromiso)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Compromiso  $compromiso
     * @return \Illuminate\Http\Response
     */
    public function edit(Compromiso $compromiso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Compromiso  $compromiso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Compromiso $compromiso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Compromiso  $compromiso
     * @return \Illuminate\Http\Response
     */
    public function destroy(Compromiso $compromiso)
    {
        //
    }
}
