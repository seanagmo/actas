<?php

namespace App\Http\Controllers;
use PDF;
use App\Models\Actas;
use Illuminate\Http\Request;

class ActasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = Actas::get();
        return response()->json($records);
    }

    public function getActa($id)
    {
        $records = Actas::whereId($id)->get();
        return response()->json($records);
    }

    public function generateReportPDF($id)
    {
        $data = Actas::whereId($id)->get();
        $info = array(json_decode($data[0]['listado_compromisos']));
        // $data[0]['listado_compromisos'] = $info;
        // $data[0]['listado_asistentes']  = $info;
        $pdf = PDF::loadView('formatos.actas', $data[0]);
    
        return $pdf->stream('formatos');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request['id'];
        if( isset($id) )
        {
            $record = Actas::whereId($id)->update($request->all());
            return response()->json(['mensaje' => 'Actualizado correctamente', $record]);
        } else {
            $record = Actas::create($request->all());
            return response()->json(['mensaje' => 'Guardado correctamente', $record]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function show(Actas $actas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function edit(Actas $actas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Actas $actas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Actas  $actas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Actas $actas)
    {
        //
    }
}
