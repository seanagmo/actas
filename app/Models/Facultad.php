<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facultad extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'facultads';

    protected $dates = ['deleted_at'];

    protected $fillable = ['facultad'];


    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'facultad' => 'string'
    ];

    public static $rules = [
        'facultad' => 'required'
    ];
}
