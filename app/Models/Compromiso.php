<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Compromiso extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'compromisos';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'descripcion',
        'fecha_de_realizacion',
        'hora_de_inicio',
        'hora_de_finalizacion',
        'estado',
        'idresponsable'
    ];

    public function responsableA()
    {
        return $this->belongsTo(User::class, 'idresponsable');
    }

    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'descripcion' => 'string',
        'fecha_de_realizacion' => 'date:Y-m-d',
        'hora_de_inicio' => 'datetime:H:i',
        'hora_de_finalizacion' => 'datetime:H:i',
        'estado' => 'string',
        'idresponsable' => 'integer'
    ];

    public static $rules = [
        'descripcion' => 'required',
        'fecha_de_realizacion' => 'required',
        'hora_de_inicio' => 'required',
        'hora_de_finalizacion' => 'required',
        'estado' => 'required',
        'idresponsable' => 'required'
    ];
}
