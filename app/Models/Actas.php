<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Actas extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'actas';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'fecha_de_realizacion', 
        'hora_de_inicio', 
        'hora_de_finalizacion', 
        'asunto', 
        'descripcion', 
        'estado', 
        'listado_asistentes', 
        'listado_compromisos',
    ];


    protected $hidden = [
    'deleted_at',
    'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'fecha_de_realizacion' => 'date:Y-m-d',
        'hora_de_inicio'       => 'datetime:H:i', 
        'hora_de_finalizacion' => 'datetime:H:i', 
        'asunto'               => 'string', 
        'descripcion'          => 'string', 
        'estado'               => 'string', 
        'listado_asistentes'    => 'string', 
        'listado_compromisos'  => 'string'
    ];

    public static $rules = [
        'fecha_de_realizacion' => 'required',
        'hora_de_inicio' => 'required', 
        'hora_de_finalizacion' => 'required', 
        'asunto' => 'required', 
        'descripcion' => 'required', 
        'estado' => 'required', 
        'listado_asistentes' => 'required', 
        'listado_compromisos'=> 'required'
    ];
}
