<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asistentes extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'asistentes';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'tipo_documento',
        'documento',
        'nombre_completo',
        'correo',
        'telefono'
    ];


    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'tipo_documento' => 'string',
        'documento' => 'string',
        'nombre_completo' => 'string',
        'correo' => 'string',
        'telefono' => 'string'
    ];

    public static $rules = [
        'tipo_documento' => 'required',
        'documento' => 'required',
        'nombre_completo' => 'required',
        'correo' => 'required',
        'telefono' => 'required'
    ];
}
