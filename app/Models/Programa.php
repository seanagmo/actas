<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Programa extends Model
{
    use HasFactory, SoftDeletes;

    public $table = 'programas';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'idfacultad', 
        'programa',
    ];

    public function facPrograma()
    {
        return $this->belongsTo(Facultad::class, 'idfacultad');
    }


    protected $hidden = [
        'deleted_at',
        'updated_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'idfacultad'=> 'integer',
        'programas' => 'string'
    ];

    public static $rules = [
        'idfacultad' => 'required',
        'programas' => 'required'
    ];
}
