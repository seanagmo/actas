<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ActasController;
use App\Http\Controllers\AsistentesController;
use App\Http\Controllers\CompromisoController;
use App\Http\Controllers\FacultadController;
use App\Http\Controllers\ProgramaController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if ( Auth::check() ) 
    {
        return view('home');
    } else {
        return view('auth.login');
    }
});

Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('usuarios', UserController::class);
    Route::resource('facultades', FacultadController::class);
    Route::get('programas/{id}', [ProgramaController::class, 'getPrograma']);
    Route::resource('programas', ProgramaController::class);

    Route::get('compromisos/{id}', [CompromisoController::class, 'getCompromiso']);
    Route::resource('compromisos', CompromisoController::class);

    Route::get('asistentes/{id}', [AsistentesController::class, 'getAsistente']);
    Route::resource('asistentes', AsistentesController::class);

    Route::get('formatos/{id}', [ActasController::class, 'generateReportPDF']);
    Route::get('actas/{id}', [ActasController::class, 'getActa']);
    Route::resource('actas', ActasController::class);
});
// Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');