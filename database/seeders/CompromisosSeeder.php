<?php

namespace Database\Seeders;

use App\Models\Compromiso;

use Carbon\Carbon;

use Illuminate\Database\Seeder;

class CompromisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::now();

        Compromiso::create([
            'descripcion' => 'Compromiso1', 
            'fecha_de_realizacion' => Carbon::now(),
            'hora_de_inicio' => $date->toTimeString(), 
            'hora_de_finalizacion' => $date->toTimeString(),
            'estado' => 'Activo',
            'idresponsable' => 1]);
        Compromiso::create([
            'descripcion' => 'Compromiso2', 
            'fecha_de_realizacion' => Carbon::now(),
            'hora_de_inicio' => $date->toTimeString(), 
            'hora_de_finalizacion' => $date->toTimeString(),
            'estado' => 'Completo',
            'idresponsable' => 1]);
        Compromiso::create([
            'descripcion' => 'Compromiso3',
            'fecha_de_realizacion' => Carbon::now(),
            'hora_de_inicio' => $date->toTimeString(), 
            'hora_de_finalizacion' => $date->toTimeString(),
            'estado' => 'Activo',
            'idresponsable' => 1]);
        Compromiso::create([
            'descripcion' => 'Compromiso4', 
            'fecha_de_realizacion' => Carbon::now(),
            'hora_de_inicio' => $date->toTimeString(), 
            'hora_de_finalizacion' => $date->toTimeString(),
            'estado' => 'Cancelado',
            'idresponsable' => 1]);
        Compromiso::create([
            'descripcion' => 'Compromiso5', 
            'fecha_de_realizacion' => Carbon::now(),
            'hora_de_inicio' => $date->toTimeString(), 
            'hora_de_finalizacion' => $date->toTimeString(),
            'estado' => 'Activo',
            'idresponsable' => 1]);
    }
}