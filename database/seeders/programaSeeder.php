<?php

namespace Database\Seeders;
use App\Models\Programa;
use Illuminate\Database\Seeder;

class programaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Programa::create(['idfacultad' => 1, 'programa' => 'Ingeniería Ambiental']);
       Programa::create(['idfacultad' => 1, 'programa' => 'Ingeniería Mecanica']);
       Programa::create(['idfacultad' => 1, 'programa' => 'Ingeniería de Sistemas']);
       Programa::create(['idfacultad' => 1, 'programa' => 'Ingeniería de Alimentos']);
       Programa::create(['idfacultad' => 1, 'programa' => 'Ingeniería Industrial']);
       Programa::create(['idfacultad' => 2, 'programa' => 'Medicina Veterinaria y Zootecnia']);
       Programa::create(['idfacultad' => 3, 'programa' => 'Acuicultura']);
       Programa::create(['idfacultad' => 3, 'programa' => 'Ingeniería Agronómica']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Ciencias Sociales']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Educación Física, Recreación y Deporte']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Literatura  y Lengua Castellana']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Informática']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Lengua Extranjera con Énfasis en Inglés']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Ciencias Naturales y Educación Ambiental']);
       Programa::create(['idfacultad' => 4, 'programa' => 'Licenciatura en Educación Infantil']);
       Programa::create(['idfacultad' => '5', 'programa' => 'Derecho']);
       Programa::create(['idfacultad' => '5', 'programa' => 'Administración en Finanzas y Negocios Internacionales']);
       Programa::create(['idfacultad' => '6', 'programa' => 'Administración en Salud']);
       Programa::create(['idfacultad' => '6', 'programa' => 'Bacteriología']);
       Programa::create(['idfacultad' => '6', 'programa' => 'Enfermería']);
       Programa::create(['idfacultad' => '6', 'programa' => 'Tecnología en Regencia y Farmacia']);
       Programa::create(['idfacultad' => '7', 'programa' => 'Estadística']);
       Programa::create(['idfacultad' => '7', 'programa' => 'Matemáticas']);
       Programa::create(['idfacultad' => '7', 'programa' => 'Geografía']);
       Programa::create(['idfacultad' => '7', 'programa' => 'Física']);
       Programa::create(['idfacultad' => '7', 'programa' => 'Química']);
       Programa::create(['idfacultad' => '7', 'programa' => 'Biología']);

    }
}
