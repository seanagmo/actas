<?php
namespace Database\Seeders;
use App\Models\Asistentes;
use Illuminate\Database\Seeder;
class AsistentesSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Asistentes::create(['tipo_documento' => 'CC', 'documento' => 121, 
                            'nombre_completo' => 'Asistente1', 'correo' => 'correo1@mail.com',
                            'telefono' => 3121]);
        Asistentes::create(['tipo_documento' => 'CC', 'documento' => 122, 
                            'nombre_completo' => 'Asistente2', 'correo' => 'correo2@mail.com',
                            'telefono' => 3122]);
        Asistentes::create(['tipo_documento' => 'CC', 'documento' => 123, 
                            'nombre_completo' => 'Asistente3', 'correo' => 'correo3@mail.com',
                            'telefono' => 3123]);
        Asistentes::create(['tipo_documento' => 'CC', 'documento' => 124, 
                            'nombre_completo' => 'Asistente4', 'correo' => 'correo4@mail.com',
                            'telefono' => 3124]);
        Asistentes::create(['tipo_documento' => 'CC', 'documento' => 125, 
                            'nombre_completo' => 'Asistente5', 'correo' => 'correo5@mail.com',
                            'telefono' => 3125]);
    }
}