<?php

namespace Database\Seeders;

use App\Models\Facultad;

use Illuminate\Database\Seeder;

class FacultadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Facultad::create(['facultad' => 'Facultad de Ingenierías']);
        Facultad::create(['facultad' => 'Facultad Medicina Veterinaria y Zootecnia']);
        Facultad::create(['facultad' => 'Facultad de Ciencias Agrícolas']);
        Facultad::create(['facultad' => 'Facultad Educación y Ciencias Humanas']);
        Facultad::create(['facultad' => 'Facultad de Ciencias Económicas, Jurídicas y Administrativas']);
        Facultad::create(['facultad' => 'Facultad de Ciencias de la Salud']);
        Facultad::create(['facultad' => 'Facultad de Ciencias Basicas']);
    }
}
