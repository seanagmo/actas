<?php
namespace Database\Seeders;
use App\Models\Actas;
use Illuminate\Database\Seeder;
class ActasSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        Actas::create(['asunto' => 'Acta 1','descripcion' => 'Acta 1',
                        'estado' => 'activo', 'listado_asistentes' => 1,2,3,4,5]);
    }
}