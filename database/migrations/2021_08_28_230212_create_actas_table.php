<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actas', function (Blueprint $table) {
            $table->id();            
            $table->date('fecha_de_realizacion');
            $table->time('hora_de_inicio');
            $table->time('hora_de_finalizacion');
            $table->string('asunto');
            $table->string('descripcion', 2000);
            $table->string('estado');
            $table->text('listado_asistentes');
            $table->text('listado_compromisos');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actas');
    }
}
