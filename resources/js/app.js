import Vue from 'vue'
import VueRouter from 'vue-router'
import swal from 'sweetalert';
import moment from "moment"
import vSelect from 'vue-select'
import Vue2Editor from "vue2-editor";
import VCalendar from 'v-calendar';


moment.locale('es');

Vue.use(Vue2Editor);
Vue.use(VCalendar);

Vue.component('v-select', vSelect)
Vue.component("datatable", require("./DataTable.vue").default);


window.Vue = require('vue').default;
window.axios = require('axios');
Vue.prototype.$http = window.axios;
axios.defaults.headers = {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
    'accept' : 'application/json'
};

Vue.use(VueRouter)

import HomeComponent from './components/home/HomeComponent.vue'

import ActasComponent from './components/acta/ActasComponent.vue'
import ActualizarActas from './components/acta/ActualizarActas.vue'
import CrearActas from './components/acta/CrearActas.vue'
import mostrarActa from './components/acta/mostrarActa.vue'

import AsistentesComponent from './components/asistentes/AsistentesComponent.vue'
import CrearAsistentes from './components/asistentes/CrearAsistentes.vue'
import ActualizarAsistentes from './components/asistentes/ActualizarAsistentes.vue'

import FacultadComponent from './components/facultad/FacultadComponent.vue'
import ActualizarFacultad from './components/facultad/ActualizarFacultad.vue'
import CrearFacultad from './components/facultad/CrearFacultad.vue'

import CompromisosComponent from './components/compromiso/CompromisosComponent.vue'
import CrearCompromiso from './components/compromiso/CrearCompromiso.vue'
import ActualizarCompromiso from './components/compromiso/ActualizarCompromiso.vue'


import Programacomponent from './components/programa/Programacomponent.vue'
import CrearPrograma from './components/programa/CrearPrograma.vue'
import ActualizarPrograma from './components/programa/ActualizarPrograma.vue'

import App from './components/App.vue'
const router = new VueRouter({
    routes: [
        {
            path: '',
            name: 'dashboard',
            component: HomeComponent
        },
        {
            path: '/actas',
            name: 'actas',
            component: ActasComponent
        },
        {
            path: '/acta/crear',
            name: 'crear-acta',
            component: CrearActas
        },
        {
            path: '/acta/actualizar/:id',
            name: 'actualizar-acta',
            component: ActualizarActas
        },
        {
            path: '/acta/ver/:id',
            name: 'ver-acta',
            component: mostrarActa
        },
        {
            path: '/compromisos',
            name: 'compromisos',
            component: CompromisosComponent
        },
        {
            path: '/compromiso/crear',
            name: 'crear-compromiso',
            component: CrearCompromiso
        },
        {
            path: '/compromiso/actualizar/:id',
            name: 'actualizar-compromiso',
            component: ActualizarCompromiso
        },
        {
            path: '/asistentes',
            name: 'asistentes',
            component: AsistentesComponent
        },
        {
            path: '/asistentes/crear',
            name: 'agregar-asistentes',
            component: CrearAsistentes
        },
        {
            path: '/asistente/actualizar/:id',
            name: 'actualizar-asistente',
            component: ActualizarAsistentes
        },
        {
            path: '/facultades',
            name: 'facultades',
            component: FacultadComponent
        },
        {
             path: '/facultades/crear',
             name: 'crear-facultad',
             component: CrearFacultad
        },
        {
            path: '/facultades/actualizar/:id',
            name: 'actualizar-facultad',
            component: ActualizarFacultad
        },
        {
            path: '/programas',
            name: 'programas',
            component: Programacomponent
        },
        {
            path: '/programas/crear',
            name: 'crear-programa',
            component: CrearPrograma
        },
        {
            path: '/programas/actualizar/:id',
            name: 'actualizar-programa',
            component: ActualizarPrograma
        }
    ]
});
const app = new Vue({
    components: {
        'app-component': App
    },
    router,
    el: '#app',
});
