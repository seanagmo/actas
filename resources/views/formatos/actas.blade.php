<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table {
            margin: auto;
            width: 100%; 
            border: 1px solid #000;
            border-collapse: collapse;
            margin-top: 10px;
        }
        tr {
            border: 1px solid #000;
            border-collapse: collapse;
        }
        td {
            border: 1px solid #000;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
    <img 
        src="https://revistas.unicordoba.edu.co/public/site/images/ojsadminunicor/unicor-acreditada5.png" 
        alt="Logo"
        width="120"
        height="80">
    <table>
        <thead>
            <tr>
                <td colspan="2" align="center"><b>ACTA DE SEGUIMIENTO</b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Fecha</td>
                <td> {{ $fecha_de_realizacion }} </td>
            </tr>
            <tr>
                <td>Hora de inicio</td>
                <td> {{ $hora_de_inicio }} </td>
            </tr>
            <tr>
                <td>Hora de finalización</td>
                <td> {{ $hora_de_finalizacion }} </td>
            </tr>
            <tr>
                <td>Asunto</td>
                <td> {{ $asunto }} </td>
            </tr>
            <tr>
                <td>Descripción</td>
                <td> {{ $descripcion }} </td>
            </tr>
            <tr>
                <td>Estado</td>
                <td> {{ $estado }} </td>
            </tr>
            <tr>
                <td align="center">Asistentes</td>
                <td>
                    <ul>           
                    </ul>
                </td>
            </tr>
            <tr>
                <td align="center">Compromisos</td>
                <td>
                    <ul>           
                    </ul>
                </td>
            </tr>
        </tbody>
    </table>
    
</body>
</html>